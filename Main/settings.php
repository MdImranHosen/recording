<?php
@ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Androcat</title>

        <!--external files for settings-->
        <link href="css/view.css" rel="stylesheet">
        <script src="js/view.js"></script>


        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/datepicker3.css" rel="stylesheet">
        <link href="css/bootstrap-table.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <!--Icons-->
        <script src="js/lumino.glyphs.js"></script>



        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="http://pdl007.com/system/"><span>Androcat</span></a>
                    <ul class="user-menu">
                        <li class="dropdown pull-right">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php
                               
                                echo htmlentities($_SESSION['email'] . " ")
                                ?><span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                <li><a href="logout.php"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div><!-- /.container-fluid -->
        </nav>

        <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
            <form role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </form>
            <ul class="nav menu">
                <li ><a href=http://pdl007.com/system/Main><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> SMS</a></li>
                <li><a href="Contacts.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> CONTACTS</a></li>
                <li><a href="Call_recore.php"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-star"></use></svg>CALL RECORD</a></li>
                <li><a href="menual_record.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg>MANUAL RECORD</a></li>
                <li><a href="images.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg>IMAGES</a></li>
                <li><a href="location.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Location</a></li>


                <li role="presentation" class="divider"></li>
                <li class="active"><a href="settings.php"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Settings</a></li>
            </ul>

        </div><!--/.sidebar-->

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
            <div class="row">
                <ol class="breadcrumb">

                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">

                    <div class="w3-progress-container">

                        <?php 
	
	
   $time;
   $position;
    //open connection to mysql db
        $mobile_id= $_SESSION['username'];
    
           
         
	 $sql ="SELECT  `UPDATE_TIME`, `BATTERY_LEVEL` FROM `mobile_details` WHERE `MOBILE_DETAILS_ID`='$mobile_id'";

	require_once('dbConnect.php');
	 $result = mysqli_query($con,$sql) ;

        if($row = mysqli_fetch_array($result)) {
         $time=$row[0];
         $position=$row[1];
     
      }
	
	
	echo  "<h4>Battery At : $time</h4>";
 
  echo "<div id='myBar' class='w3-progressbar w3-green' style='width:$position%'>";
  echo "<div id='demo' class='w3-center w3-text-white'>$position %</div>";
  ?>

                    </div>
                </div>

                <?php
                $mobile_id = $_SESSION['username'];
              
                $sql ="SELECT `CURRENT_SETTINGS`,WHITELIST_NUMBERS FROM `mobile_details` WHERE `MOBILE_DETAILS_ID`='$mobile_id'";
                require_once('dbConnect.php');
                $result = mysqli_query($con, $sql);

                if ($row = mysqli_fetch_array($result)) {
                    $all_settings = $row[0];
                   $whitelistnumbers=$row[1];
                    $settings = explode("@", $row[0]);

                    $image = $settings[0];
                    $socket = $settings[1];
                    $sim_change = $settings[2];
                    $audio_source = $settings[3];
                    $upload_command = $settings[4];
                    $sim_c_com = $settings[5];
                    $icon_hide_unhide = $settings[6];
                   $image_qulity=$settings[7];



                    if ($icon_hide_unhide == "1") {
                        $icon = "checked";
                    } else {
                        $icon = "";
                    }


                    if ($sim_c_com == "1") {
                        $sim_c = "checked";
                    } else {
                        $sim_c = "";
                    }


                    if ($upload_command == "00") {

                        $file_up = "";
                        $file_del = "";
                    } else if ($upload_command == "11") {
                        $file_up = "checked";
                        $file_del = "checked";
                    } else if ($upload_command == "10") {
                        $file_up = "checked";
                        $file_del = "";
                    }

                }
                ?>


                <div id="main_body" >                 

                    <br/>  <br/>  <br/>
                    <div id="form_container">

                        <!--<h1><a></a></h1>-->
                        <form id="form_1136906" class="appnitro"  method="post" action="change_pass.php">
                            <div class="form_description">
                                <h2>Change Password</h2>

                            </div>

                            <ul >

                                <li id="li_1" >
                                    <label class="description" for="element_1">Current Password </label>
                                    <div>
                                        <input id="element_1" name="element_1" class="element text medium" type="password" maxlength="255" required/> 
                                    </div><p class="guidelines" id="guide_1"><small>Write here your current password</small></p> 
                                </li>		<li id="li_2" >
                                    <label class="description" for="element_2">New Password </label>
                                    <div>
                                        <input id="element_2" name="element_2" class="element text medium" type="password" maxlength="255" value="" required/> 
                                    </div><p class="guidelines" id="guide_2"><small>Write here your new password</small></p> 
                                </li>		<li id="li_3" >
                                    <label class="description" for="element_3">Confirm Password </label>
                                    <div>
                                        <input id="element_3" name="element_3" class="element text medium" type="password" maxlength="255" value="" required/> 
                                    </div><p class="guidelines" id="guide_3"><small>Write here your confirm password</small></p> 
                                </li>

                                <li class="buttons">
                                    <input type="hidden" name="form_id" value="1136906" />

                                    <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                                </li>
                            </ul>
                        </form>	                     
                    </div>

                    <br/>

                    <div id="form_container">
                        <form id="form_1137873" class="appnitro"  method="post" action="settings/image_setting.php">
                            <div class="form_description">
                                <h2>Image Settings</h2>
                                <p></p>
                            </div>						
                            <ul >

                                <li id="li_2" >
                                    <label class="description" for="element_2">Capture Image </label>
                                    <span>
                                        <input id="element_2_1" name="element_2_1" class="element checkbox" type="checkbox" value="1" <?php if($image != "00"){ echo "checked";} ?>/>

                                        <label class="choice" for="element_2_1">ON/OFF</label>

                                    </span> 
                                </li>		<li id="li_1" >
                                    <label class="description" for="element_1">Select  Camera </label>
                                    <span>
          <input id="element_1_1" name="element_1" class="element radio" type="radio" value="1"  <?php if($image == "11"){echo "checked";} ?>/>
                                        <label class="choice" for="element_1_1">Front</label>
     <input id="element_1_2" name="element_1" class="element radio" type="radio" value="2"  <?php if($image == "12"){echo "checked";} ?>/>
                                        <label class="choice" for="element_1_2">Back</label>

                                    </span> 
                                </li>

<li id="li_1" >
		<label class="description" for="elem">Image qulity </label>
		<div>
		<select class="element select medium" id="element_1" name="element_5"> 
         <option value="1" <?php if ( $image_qulity == "1") {
                                                echo "selected=selected";
                                            }
                                            ?> >Low</option>
        <option value="2" <?php if ( $image_qulity == "2") {
                                                echo "selected=selected";
                                            }
                                            ?>>Medium</option>
        <option value="3" <?php if ( $image_qulity == "3") {
                                                echo "selected=selected";
                                            }
                                            ?>>High</option>

		</select>
		</div> 
		</li>

                                <li class="buttons">
                                    <input type="hidden" name="form_id" value="1137873" />

                                    <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                                </li>
                            </ul>
                        </form>	
                    </div>
                    <br/>
                 
                    <div id="form_container">

                        <form id="form_1137638" class="appnitro"  method="post" action="settings/sim_change_setting.php">
                            <div class="form_description">
                                <h2>Sim Change</h2>
                            </div>						
                            <ul >
                                <li id="li_2" >
                                    <label class="description" for="element_2">Sim change notification </label>
                                    <span>
                                        <input id="element_2_1" name="element_2_1" class="element checkbox" type="checkbox" value="1" <?php echo $sim_c; ?>/>
                                        <label class="choice" for="element_2_1">OFF/ON</label>

                                    </span> 
                                </li>

                                Current Number : <?php echo $sim_change; ?>
                                <li id="li_1" >
                                    <label class="description" for="element_1">Sim cahnge notification number </label>
                                    <div>
                                        <input id="element_1" name="element_1" class="element text medium" type="text" maxlength="255" value=""/> 
                                    </div> 
                                </li>

                                <li class="buttons">
                                    <input type="hidden" name="form_id" value="1137638" />

                                    <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                                </li>
                            </ul>
                        </form>	
                    </div>
                    <br/>
                    <div id="form_container">

                        <form id="form_1137638" class="appnitro"  method="post" action="settings/audio_settings.php">
                            <div class="form_description">
                                <h2>Audio</h2>
                                <p></p>
                            </div>						
                            <ul >

                                <li id="li_1" >
                                    <label class="description" for="element_1">Source </label>
                                    <div>
                                        <select class="element select medium" id="element_1" name="element_1"> 

                                            <option value="1" <?php
                                            if ($audio_source == "1") {
                                                echo "selected=selected";
                                            }
                                            ?>>MIC</option>
                                            <option value="2" <?php
                                            if ($audio_source == "2") {
                                                echo "selected=selected";
                                            }
                                            ?>">VOICE_CALL</option>
                                            <option value="3" <?php
                                            if ($audio_source == "3") {
                                                echo "selected=selected";
                                            }
                                            ?>">REMOTE_SUBMIX</option>
                                            <option value="4" <?php
                                            if ($audio_source == "4") {
                                                echo "selected=selected";
                                            }
                                            ?>">VOICE_COMMUNICATION</option>
                                            <option value="5" <?php
                                            if ($audio_source == "5") {
                                                echo "selected=selected";
                                            }
                                            ?>">VOICE_DOWNLINK</option>
                                            <option value="6" <?php
                                            if ($audio_source == "6") {
                                                echo "selected=selected";
                                            }
                                            ?>">VOICE_RECOGNITION</option>
                                            <option value="7" <?php
                                            if ($audio_source == "7") {
                                                echo "selected=selected";
                                            }
                                            ?>">VOICE_UPLINK</option>
                                            <option value="8" <?php
                                            if ($audio_source == "8") {
                                                echo "selected=selected";
                                            }
                                            ?>">CAMCORDER</option>
                                            <option value="9" <?php
                                            if ($audio_source == "9") {
                                                echo "selected=selected";
                                            }
                                            ?>">DEFAULT</option>
                                        </select>
                                    </div> 
                                </li>

                                <li class="buttons">
                                    <input type="hidden" name="form_id" value="1137638" />

                                    <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                                </li>
                            </ul>
                        </form>	
                    </div>
                    <br/>
                    <div id="form_container">

                        <form id="form_1138777" class="appnitro"  method="post" action="settings/file_upload_setting.php">
                            <div class="form_description">
                                <h2>File</h2>

                            </div>						
                            <ul >

                                <li id="li_1" >
                                    <label class="description" for="element_1">File upload to server </label>
                                    <span>
                                        <input id="element_1_1" name="element_1_1" class="element checkbox" type="checkbox" value="1"  <?php echo $file_up; ?>/ >
                                               <label class="choice" for="element_1_1">OFF/ON</label>

                                    </span> 
                                </li>		
                                <li id="li_2" >
                                    <label class="description" for="element_2">Delete after Upload </label>
                                    <span>
                                        <input id="element_2_1" name="element_2_1" class="element checkbox" type="checkbox" value="1" <?php echo $file_del; ?>/>
                                        <label class="choice" for="element_2_1">OFF/ON</label>

                                    </span> 
                                </li>

                                <li class="buttons">
                                    <input type="hidden" name="form_id" value="1138777" />

                                    <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                                </li>
                            </ul>
                        </form>	
                    </div>
                 

                    <!--Alert configaration forma-->
                    <br/>
                    <div id="form_container">

                        <!--<h1><a></a></h1>-->
                        <form id="whitelistform" class="appnitro" onsubmit="return validateForm()"  method="post" action="settings/whitelist_settings.php">
                            <div class="form_description">
                                <h2>White list : </h2>
                                <p >Incoming and outgoing calls from white listed numbers will not be recorded </p>


                            </div>


                            <li id="li_2" >
                                <input id="specialcb" type="checkbox" name="checkAddress" onChange="enableEdit()" > Edit existing Numbers </checkbox> <br/>
                                <textarea id="oldnos" name="olds" cols="53" readonly>
                                    <?php echo $whitelistnumbers; ?>
                                </textarea>
                            </li>


                            <li id="li_5" >
                                <label class="description" for="number">Numbers to be added:  </label>
                                <div>
                                    <input id="number" name="number" class="element text medium" type="text" maxlength="130" value="" /> 
                                </div><p class="guidelines" id="guide_5"><small>Multiple numbers should be separated with comma ',' </small></p> 
                            </li>






                            <li class="buttons">
                                  <!--                        <input type="hidden" name="form_id" value="1135092" />-->
                                <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
                            </li>

                        </form>	

                        <script>


                             function enableEdit() {
                                    var editcb = document.forms["whitelistform"]["checkAddress"].checked;
                                    if (editcb === true) {
//                                        alert("Enable");
                                        document.forms["whitelistform"]["olds"].readOnly= false;
                                    }else {
//                                        alert("Disable");
                                        document.forms["whitelistform"]["olds"].readOnly = true;
                                    }
                                }
                            





                            function validateForm() {

                                var number = document.forms["whitelistform"]["number"].value;
                                var olds = document.forms["whitelistform"]["olds"].value;


                                if (number == null || number == "" || number == "0") {
                                    alert("number must be filled out");
                                    return false;
                                }
                                if (number.indexOf(' ') >= 0) {
                                    alert("Number should not contain space");
                                    return false;
                                }
                                if (olds.trim().indexOf(' ') >= 0) {
                                    alert("Old numbers should not contain space");
                                    return false;
                                }







                                return true;
                            }
                        </script>
                    </div>

                </div>


            </div>
        </div><!--/.row-->


        <script>
            $(function () {
                $('#hover, #striped, #condensed').click(function () {
                    var classes = 'table';

                    if ($('#hover').prop('checked')) {
                        classes += ' table-hover';
                    }
                    if ($('#condensed').prop('checked')) {
                        classes += ' table-condensed';
                    }
                    $('#table-style').bootstrapTable('destroy')
                            .bootstrapTable({
                                classes: classes,
                                striped: $('#striped').prop('checked')
                            });
                });
            });

            function rowStyle(row, index) {
                var classes = ['active', 'success', 'info', 'warning', 'danger'];

                if (index % 2 === 0 && index / 2 < classes.length) {
                    return {
                        classes: classes[index / 2]
                    };
                }
                return {};
            }
        </script>


        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-table.js"></script>
        <script>
            !function ($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768)
                    $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function () {
                if ($(window).width() <= 767)
                    $('#sidebar-collapse').collapse('hide')
            })
        </script>

    </body>
</html>